import { Component } from "@angular/core";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html"
})
export class AppComponent {
    onTap(e) {
        let toastOptions: Toast.ToastOptions = {
            text: "Native code",
            duration: Toast.DURATION.SHORT,
            position: Toast.POSITION.BOTTOM
        };
        Toast.show(toastOptions);
    }
}
